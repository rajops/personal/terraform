# resource block for VPC creation #
resource "aws_vpc" "VPC-1" {
    cidr_block = "10.0.0.0/16"
    instance_tenancy = "default"
    enable_dns_support = true
    tags    =   {
        name    =   "VPC-1"
    }
}