# resource block for IGW attachement #
resource "aws_internet_gateway_attachment" "igw-attach" {
    internet_gateway_id = aws_internet_gateway.IGW-VPC-1.id
    vpc_id = aws_vpc.VPC-1.id
}