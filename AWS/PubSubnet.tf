# resource block for Subnet creation #
resource "aws_subnet" "pubsub1-vpc-1" {
    cidr_block = "10.0.0.0/24"
    vpc_id  =   aws_vpc.VPC-1.id
}
resource "aws_subnet" "pubsub2-vpc-1" {
    cidr_block = "10.0.10.0/24"
    vpc_id  =   aws_vpc.VPC-1.id
}