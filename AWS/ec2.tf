# resource block for EC2 creation #
resource "aws_instance" "EC2_Frontend" {
    instance_type = "t2.micro"
    ami = "ami-0889a44b331db0194"
    key_name = "key"
    tags = {
      name = "Frontend-Server"
    }
}
# resource block for EC2 creation #
resource "aws_instance" "EC2_backend" {
    instance_type = "t2.micro"
    ami = "ami-0889a44b331db0194"
    key_name = "key"
    tags = {
      name = "backend-Server"
    }
}